/**
 * popula lista de filmes modal geral
 * 
 * @param xmlList
 *            lista de filmes disponíveis
 */
function filmes(xmlList) {

	var i = 0;
	var listContainer = null;
	listContainer = $('#list');

	listContainer
			.prepend('<thead id="head"><tr><th width="20%"></th><th width="20%">Título do Filme</th><th width="5%">Ano</th><th width="50%">Descrição</th><th width="5%">Assistir</th><th></th></tr></thead>');

	for (i = 0; i < xmlList.length; i++) {
		if (type === xmlList[i].getElementsByTagName("categoria")[0].childNodes[0].nodeValue) {
			inputValue = $('#input').val();
			// add new list item
			listContainer
					.prepend('<tr id="linha"><td><img class="img-circle" src='
							+ xmlList[i].getElementsByTagName("urlImagem")[0].childNodes[0].nodeValue
							+ ' alt="" height="100" width="100"></td><td>'
							+ xmlList[i].getElementsByTagName("nome")[0].childNodes[0].nodeValue
							+ '</td><td>'
							+ xmlList[i].getElementsByTagName("ano")[0].childNodes[0].nodeValue
							+ '</td><td>'
							+ xmlList[i].getElementsByTagName("descricao")[0].childNodes[0].nodeValue
							+ '</td> <td align="center"><a href="#" data-toggle="modal" data-target="#playMovieUnfinished"><i class="fa fa-video-camera"></i></a></td></tr>');
			// clear value input
			$('#input').val('');
		}
	}

}
var type;

/**
 * GoHorseProcess mode filtrar tipo de filmes
 * 
 * @param movieType
 *            tipo de filme selecionado
 */
function gambOnGHP(movieType) {
	type = "";
	type = $("#" + movieType).attr("name");
	loadDoc();
}

/**
 * popula modal de filmes vistos
 */
function watch(xmlList) {

	var i = 0;
	var listContainer = $('#listUnfinished');

	listContainer
			.prepend('<thead ><tr><th width="20%"></th><th width="20%">Título do Filme</th><th width="5%">Ano</th><th width="50%">Descrição</th><th width="5%">Assistir</th><th></th></tr></thead>');

	for (i = 0; i < xmlList.length; i++) {
		inputValue = $('#input').val();
		// add new list item
		listContainer
				.prepend('<tr><td><img class="img-circle" src='
						+ xmlList[i].getElementsByTagName("urlImagem")[0].childNodes[0].nodeValue
						+ ' alt="" height="100" width="100"></td><td>'
						+ xmlList[i].getElementsByTagName("nome")[0].childNodes[0].nodeValue
						+ '</td><td>'
						+ xmlList[i].getElementsByTagName("ano")[0].childNodes[0].nodeValue
						+ '</td><td>'
						+ xmlList[i].getElementsByTagName("descricao")[0].childNodes[0].nodeValue
						+ '</td> <td align="center"><a href="#" data-toggle="modal" data-target="#playMovieUnfinished"><i class="fa fa-video-camera"></i></a></td></tr>');
		// clear value input
		$('#input').val('');
	}

}
/**
 * busca lista de filmes
 */
function loadDoc() {
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
			this.xmlResponse = getXML(xhttp);
		}
	};
	xhttp.open("GET", "http://localhost:8085/Servidor/rest/filmes", true);
	xhttp.send();
}

function loadUser(usuario, senha) {
	var userRequest = new XMLHttpRequest();
	userRequest.onreadystatechange = function() {
		if (userRequest.readyState == 4 && userRequest.status == 200) {
			this.xmlResponse = getXML(userRequest);
		}
	};
	userRequest.open("GET", "http://localhost:8085/Servidor/rest/usuario/"+usuario+"/"+senha, true);
	userRequest.send();
}

/**
 * popula listas com filmes de acordo com parametros necessarios
 * 
 * @param xml
 *            xml do servidor
 * @returns
 */
function getXML(xml) {
	var i;
	var xmlDoc = "";
	xmlDoc = xml.responseXML;
	var xmlList = xmlDoc.getElementsByTagName("filme");

	watch(xmlList);

	filmes(xmlList);

	return xmlList;
}

/**
 * remoção de filmes do modal após fechar
 */
$("#modalMovies").on('hide.bs.modal', function() {

	$("#list tr").each(function(){
		$(this).remove();
	});
	
});

$("#modalMovies").on('hide.bs.modal', function() {

	$("#list tr").each(function(){
		$(this).remove();
	});
	
});

$("#modalMoviesUnfinished").on('hide.bs.modal', function() {

	$("#listUnfinished tr").each(function(){
		$(this).remove();
	});
});

function timeController(){
	var video = document.getElementById("unfinishedMovie");//playVideo
	video.pause();
	var duracao = video.duration;
	var atual = video.currentTime;
	if(atual <= (duracao - 5)){
		alert(video.currentTime);
	}
}
