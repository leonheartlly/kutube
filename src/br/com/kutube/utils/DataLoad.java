package br.com.kutube.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class DataLoad {
	
	public static String caminho;

	/**
	 * Obtem lista de valores do arquivo de propriedades
	 * @param nome arquivo de propriedades
	 * @return lista de valores
	 */
	public static String getProperties(String key) {

		Properties props = new Properties();
		FileInputStream file;
		try {
			file = new FileInputStream("/Users/luisteodoro/Documents/workspace/Servidor2/properties/userData.properties");
			props.load(file);
			caminho = props.getProperty(key);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return caminho;
	}

	

}
