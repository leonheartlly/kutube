package br.com.k19.models;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RespostaPadrao {
	
	private String mensagem;

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

}
