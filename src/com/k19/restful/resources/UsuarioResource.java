package com.k19.restful.resources;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import br.com.k19.models.RespostaPadrao;
import br.com.k19.models.Usuario;
import br.com.kutube.utils.DataLoad;

@Path("/usuario")
public class UsuarioResource {

	static private List<Usuario> listaUser;

	public UsuarioResource() throws FileNotFoundException, IOException,
			ClassNotFoundException {

		ObjectInputStream objectInputStream = new ObjectInputStream(
				new FileInputStream(DataLoad.getProperties("usuario")));
		// write a date"));

		// start getting the objects out in the order in which they were written
		Date date = (Date) objectInputStream.readObject();
		System.out.println(date);
		System.out.println(objectInputStream.readBoolean());
		System.out.println(objectInputStream.readFloat());

		// get the course object
		listaUser = (ArrayList<Usuario>) objectInputStream.readObject();

	}

	@Path("{login}/{senha}")
	@GET
	@Produces({ "text/xml", "application/json" })
	public Response cadastraUser(@PathParam("login") String login,
			@PathParam("senha") String senha) throws IOException,
			ClassNotFoundException {

		System.out.println(login);
		System.out.println(senha);

		ObjectOutputStream objectOutputStream = new ObjectOutputStream(
				new FileOutputStream(DataLoad.getProperties("usuario")));
		objectOutputStream.writeObject(new Date());
		objectOutputStream.writeBoolean(true);
		objectOutputStream.writeFloat(1.0f);

		Usuario user = new Usuario();

		listaUser.size();

		user.setId(listaUser.size() + 1);
		user.setLogin(login);
		user.setSenha(senha);

		listaUser.add(user);

		objectOutputStream.writeObject(listaUser);
		objectOutputStream.flush();
		objectOutputStream.close();

		RespostaPadrao resposta = new RespostaPadrao();
		resposta.setMensagem("Cadastro realizado com sucesso");

		return Response.status(Response.Status.OK).entity(resposta).build();
	}

	@Path("{id}")
	@GET
	@Produces({ "text/xml", "application/json" })
	public Response getuser(@PathParam("id") int id) {

		return Response.status(Response.Status.OK).entity(listaUser.get(id))
				.build();
	}


	
	/**
	 * Valida login do usuario
	 * @param user
	 * @param senha
	 * @return msg da validacao 
	 */
	@Path("/login/{user}/{senha}")
	@GET
	@Produces({ "text/xml", "application/json" })
	public Response getLogin(@PathParam("user") String user,
			@PathParam("senha") String senha) {
		boolean validaLogin = false;
		for (Usuario dados : listaUser) {
			if (dados.getLogin().equals(user) && dados.getSenha().equals(senha)) {
				validaLogin = true;
			}
		}

		if (validaLogin) {

			RespostaPadrao resposta = new RespostaPadrao();
			resposta.setMensagem("true");
			return Response.status(Response.Status.OK).entity(resposta).build();
		} else {

			RespostaPadrao resposta = new RespostaPadrao();
			resposta.setMensagem("false");
			return Response.status(Response.Status.OK).entity(resposta).build();
		}
	}

}
