package com.k19.restful.resources;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.k19.models.Filme;
import br.com.kutube.utils.DataLoad;

public class CadastroFilmes implements Serializable {
	
public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
		
		/*
		ObjectInputStream objectInputStream = new ObjectInputStream(
		        new FileInputStream("/Users/luisteodoro/Documents/workspace/Servidor/arquivo.txt"));
		// write a date"));
		 
		// start getting the objects out in the order in which they were written
		Date date = (Date) objectInputStream.readObject();
		System.out.println(date);
		System.out.println(objectInputStream.readBoolean());
		System.out.println(objectInputStream.readFloat());
		 
		// get the course object
		ArrayList<Filme> filmes = (ArrayList<Filme>) objectInputStream.readObject();
		
		
		for (Filme filme : filmes) {
			System.out.println(filme.getDescricao());
			System.out.println(filme.getNome());
		}
		

		objectInputStream.close();
		*/
		
		listar();
		
	
	}

    public static void listar() throws IOException, ClassNotFoundException {
    	
    	
	ObjectOutputStream objectOutputStream = new ObjectOutputStream(
	                new FileOutputStream(DataLoad.getProperties("caminhoFilmes")));
	// write a date
	objectOutputStream.writeObject(new Date());
	// write a boolean
	objectOutputStream.writeBoolean(true);
	// write a float
	objectOutputStream.writeFloat(1.0f);
	// the other primitive types and objects can be saved as well
	 
	// create two students objects and add them in a list. create a course
	// object and add the list of students to a list
	Filme filme2 = new Filme();
	filme2.setId(2);
	filme2.setNome("Batman");
	filme2.setAno("2015");
	filme2.setCategoria("aventura");
	filme2.setDiretores("Jobs");
	filme2.setDataLancamento("11/12/2014");
	filme2.setDuracao("110min");
	filme2.setAtores("Um monte de atores");
	filme2.setIdioma("Ingles");
	filme2.setUrlFilme("https://translate.google.com.br:");
	filme2.setUrlImagem("https://i.ytimg.com/vi/UMzFzWAL27s/maxresdefault.jpg");
	filme2.setDescricao("Ap�s os eventos de O Homem de A�o, Superman (Henry Cavill) divide a opini�o da popula��o mundial. Enquanto muitos contam com ele como her�i e principal salvador, v�rios outros n�o concordam com sua perman�ncia no planeta. Bruce Wayne (Ben Affleck) est� do lado dos inimigos de Clark Kent e decide usar sua for�a de Batman para enfrent�-lo. Enquanto os dois brigam, por�m, uma nova amea�a ganha for�a");
	filme2.setLegenda("Portugues");
	
	
	Filme filme1 = new Filme();
	filme1.setId(1);
	filme1.setNome("Avatar");
	filme1.setAno("2009");
	filme1.setCategoria("Ficcao");
	filme1.setDiretores("James Cameron");
	filme1.setDataLancamento("18/12/2009");
	filme1.setDuracao("120min");
	filme1.setAtores("Um monte de atores");
	filme1.setIdioma("Ingles");
	filme1.setUrlFilme("https://translate.google.com.br:");
	filme1.setUrlImagem("http://globalcomment.com/wp-content/uploads/2009/12/avatarstill1.jpg");
	filme1.setDescricao("No ano 2154 d.C., a corporativa humana RDA explora min�rio em Pandora, uma das luas de Polifemo, um dos tr�s gigantes gasosos fict�cios orbitando Alpha Centauri, a 4,4 anos-luz da Terra");
	filme1.setLegenda("Portugues");
	
	Filme filme3 = new Filme();
	filme3.setId(1);
	filme3.setNome("Robocop");
	filme3.setAno("2014");
	filme3.setCategoria("acao");
	filme3.setDiretores("Joao Machado");
	filme3.setDataLancamento("12/10/2015");
	filme3.setDuracao("90min");
	filme3.setAtores("Um monte de atores");
	filme3.setIdioma("Ingles");
	filme3.setUrlFilme("https://translate.google.com.br:");
	filme3.setUrlImagem("http://nicktiffany.com/wp-content/uploads/2013/09/robocop_2014_movie-1920x1440.jpg");
	filme3.setDescricao("Em um futuro n�o muito distante, no ano de 2028, drones n�o tripulados e rob�s s�o usados para garantir a seguran�a mundo afora, mas o combate ao crime nos Estados Unidos n�o pode ser realizado por eles e a empresa OmniCorp, criadora das m�quinas, quer reverter esse cen�rio");
	filme3.setLegenda("Portugues");



	List<Filme> filmes = new ArrayList<>();
	filmes.add(filme2);
	filmes.add(filme1);
	filmes.add(filme3);

	 
	// write the course object to the objectoutputstream. This writes the
	// object as well all objects that it referes to.
	// it writes only those objects that implement serializable
	 
	//objectOutputStream.writeObject(filme2);
	objectOutputStream.writeObject(filmes);
	objectOutputStream.flush();
	objectOutputStream.close();
	 
	
	/*
	// the object input stream reads the objects back from the file and
	// creates java objects out of them. It recreates all
	// references that were present when the objects were written
	ObjectInputStream objectInputStream = new ObjectInputStream(
	        new FileInputStream("/Users/luisteodoro/Documents/workspace/Servidor/arquivo.txt"));
	// write a date"));
	 
	// start getting the objects out in the order in which they were written
	Date date = (Date) objectInputStream.readObject();
	System.out.println(date);
	System.out.println(objectInputStream.readBoolean());
	System.out.println(objectInputStream.readFloat());
	 
	// get the course object
	Filme filme = (Filme) objectInputStream.readObject();
	System.out.println(filme.getDescricao());

	objectInputStream.close();
	
	*/
		   
	}
	
	

}
