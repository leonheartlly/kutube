package com.k19.restful.resources;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import br.com.k19.models.DadosUsuario;
import br.com.k19.models.Filme;
import br.com.kutube.utils.DataLoad;

@Path("/filmes")
public class FilmesResource {
	
	
	static private List<Filme> filmesMap;
	
	public FilmesResource()   throws FileNotFoundException, IOException, ClassNotFoundException{
		
		ObjectInputStream objectInputStream = new ObjectInputStream(
		        new FileInputStream(DataLoad.getProperties("caminhoFilmes")));
		// write a date"));
		 
		// start getting the objects out in the order in which they were written
		Date date = (Date) objectInputStream.readObject();
		System.out.println(date);
		System.out.println(objectInputStream.readBoolean());
		System.out.println(objectInputStream.readFloat());
		 
		// get the course object
		filmesMap = (ArrayList<Filme>) objectInputStream.readObject();
		
		
	}

	 

	
	@GET
	@Produces({ "text/xml", "application/json" })
	public Response getFilmes() {

	GenericEntity<List<Filme>> entity = new GenericEntity<List<Filme>>(filmesMap) {};
		return Response.status(Response.Status.OK)
				.entity(entity)
				.build();

	}
	
	
	
	@Path("{id}")
	@GET
	@Produces({ "text/xml", "application/json" })
	public Response getFilme(@PathParam("id") int id) {
		
		
		for	(Filme dados : filmesMap){
			
			if(dados.getId().equals(id)){
				return Response.status(Response.Status.OK)
						.entity(dados)
						.build();
			}
		}
		
		return null;	
	}

}
