package com.k19.restful.resources;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import br.com.k19.models.Banda;
import br.com.k19.models.DadosUsuario;
import br.com.k19.models.RespostaPadrao;
import br.com.k19.models.Usuario;
import br.com.kutube.utils.DataLoad;


@Path("/dados")
public class DadosUsuarioResource {
	
	
	
	static private List<DadosUsuario> dadosUsuario;
	
	public DadosUsuarioResource() throws FileNotFoundException, IOException, ClassNotFoundException {
		
		ObjectInputStream objectInputStream = new ObjectInputStream(
		        new FileInputStream(DataLoad.getProperties("dadosUser")));
		// write a date"));
		 
		// start getting the objects out in the order in which they were written
		Date date = (Date) objectInputStream.readObject();
		System.out.println(date);
		System.out.println(objectInputStream.readBoolean());
		System.out.println(objectInputStream.readFloat());
		 
		// get the course object
		dadosUsuario = (ArrayList<DadosUsuario>) objectInputStream.readObject();
		
	}
	
	@POST
	@Consumes({ "text/xml", "application/json" })
	@Produces({ "text/xml", "application/json" })
	public Response adicionaDadosUser(DadosUsuario dados) throws IOException, ClassNotFoundException  {
		System.out.println("Entrou aqui!");
		
		ObjectOutputStream objectOutputStream = new ObjectOutputStream(
				new FileOutputStream(DataLoad.getProperties("dadosSalvos")));
		objectOutputStream.writeObject(new Date());
		objectOutputStream.writeBoolean(true);
		objectOutputStream.writeFloat(1.0f);
				
		dados.setId(dadosUsuario.size());
		
		dadosUsuario.add(dados);
	
		objectOutputStream.writeObject(dadosUsuario);
		objectOutputStream.flush();
		objectOutputStream.close();

		RespostaPadrao resposta = new RespostaPadrao();
		resposta.setMensagem("Dados salvo com sucesso!");

		return Response.status(Response.Status.OK).entity(resposta).build();
		
	}
	
	
	@Path("{id}/{idFilmes}")
	@GET
	@Produces({ "text/xml", "application/json" })
	public Response getDados(@PathParam("id") int id, @PathParam("idFilmes") int idFilme) {
		
		for	(DadosUsuario dados : dadosUsuario){
			
			if(dados.getIdCliente().equals(id) && dados.getIdFilme().equals(idFilme) ){
				return Response.status(Response.Status.OK)
						.entity(dados)
						.build();
			}
		}
		
		return null;
	}
	
	
	
	@Path("{idCliente}/{idFilmes}/{tempo}/{volume}")
	@GET
	@Produces({ "text/xml", "application/json" })
	public Response salvaDados(@PathParam("idCliente") int idCliente, @PathParam("idFilmes") int idFilme, @PathParam("tempo") String tempo, @PathParam("volume") String volume) throws FileNotFoundException, IOException {
		
		DadosUsuario dados = new DadosUsuario();
		
		
		ObjectOutputStream objectOutputStream = new ObjectOutputStream(
				new FileOutputStream(DataLoad.getProperties("dadosSalvos")));
		objectOutputStream.writeObject(new Date());
		objectOutputStream.writeBoolean(true);
		objectOutputStream.writeFloat(1.0f);
				
		dados.setId(dadosUsuario.size());
		dados.setVolume(volume);
		dados.setTempo(tempo);
		dados.setIdFilme(idFilme);
		dados.setIdCliente(idCliente);
		
		dadosUsuario.add(dados);
	
		objectOutputStream.writeObject(dadosUsuario);
		objectOutputStream.flush();
		objectOutputStream.close();

		RespostaPadrao resposta = new RespostaPadrao();
		resposta.setMensagem("Dados salvo com sucesso!");

		return Response.status(Response.Status.OK).entity(resposta).build();
	}


}
