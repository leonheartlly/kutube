package com.k19.restful.resources;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.k19.models.Filme;
import br.com.kutube.utils.DataLoad;



public class teste implements Serializable{  
   
	public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
		
		/*
		ObjectInputStream objectInputStream = new ObjectInputStream(
		        new FileInputStream("/Users/luisteodoro/Documents/workspace/Servidor/arquivo.txt"));
		// write a date"));
		 
		// start getting the objects out in the order in which they were written
		Date date = (Date) objectInputStream.readObject();
		System.out.println(date);
		System.out.println(objectInputStream.readBoolean());
		System.out.println(objectInputStream.readFloat());
		 
		// get the course object
		ArrayList<Filme> filmes = (ArrayList<Filme>) objectInputStream.readObject();
		
		
		for (Filme filme : filmes) {
			System.out.println(filme.getDescricao());
			System.out.println(filme.getNome());
		}
		

		objectInputStream.close();
		*/
		listar();
		
	
	}

    public static void listar() throws IOException, ClassNotFoundException {
    	
      
	ObjectOutputStream objectOutputStream = new ObjectOutputStream(
	                new FileOutputStream(DataLoad.getProperties("teste")));
	// write a date
	objectOutputStream.writeObject(new Date());
	// write a boolean
	objectOutputStream.writeBoolean(true);
	// write a float
	objectOutputStream.writeFloat(1.0f);
	// the other primitive types and objects can be saved as well
	 
	// create two students objects and add them in a list. create a course
	// object and add the list of students to a list
	Filme filme2 = new Filme();
	filme2.setId(2);
	filme2.setNome("Batman");
	filme2.setAno("2015");
	filme2.setCategoria("Ação");
	filme2.setDiretores("Jobs");
	filme2.setDataLancamento("11/12/2014");
	filme2.setDuracao("110min");
	filme2.setAtores("Um monte de atores");
	filme2.setIdioma("Ingles");
	filme2.setUrlFilme("https://translate.google.com.br:");
	filme2.setUrlImagem("https://translate.google.com.br");
	filme2.setDescricao("owjeojwje qeqwo eo qo eoq eoq o eoq e q e q eoq  eoq oe q e qwoenoqwnoeq eo qw eq  eoqwenqwoejqwe wq eqwoe oqw eo wq eoqw e qwo eoqw eo q e w");
	filme2.setLegenda("Portugues");
	
	
	Filme filme1 = new Filme();
	filme1.setId(1);
	filme1.setNome("Americano Vermleho");
	filme1.setAno("2011");
	filme1.setCategoria("Ação");
	filme1.setDiretores("Abrão Linconl");
	filme1.setDataLancamento("10/10/2011");
	filme1.setDuracao("120min");
	filme1.setAtores("Um monte de atores");
	filme1.setIdioma("Ingles");
	filme1.setUrlFilme("https://translate.google.com.br:");
	filme1.setUrlImagem("https://translate.google.com.br");
	filme1.setDescricao("owjeojwje qeqwo eo qo eoq eoq o eoq e q e q eoq  eoq oe q e qwoenoqwnoeq eo qw eq  eoqwenqwoejqwe wq eqwoe oqw eo wq eoqw e qwo eoqw eo q e w");
	filme1.setLegenda("Portugues");



	List<Filme> filmes = new ArrayList<>();
	filmes.add(filme2);
	filmes.add(filme1);
	 
	// write the course object to the objectoutputstream. This writes the
	// object as well all objects that it referes to.
	// it writes only those objects that implement serializable
	 
	//objectOutputStream.writeObject(filme2);
	objectOutputStream.writeObject(filmes);
	objectOutputStream.flush();
	objectOutputStream.close();
	 
	
	/*
	// the object input stream reads the objects back from the file and
	// creates java objects out of them. It recreates all
	// references that were present when the objects were written
	ObjectInputStream objectInputStream = new ObjectInputStream(
	        new FileInputStream("/Users/luisteodoro/Documents/workspace/Servidor/arquivo.txt"));
	// write a date"));
	 
	// start getting the objects out in the order in which they were written
	Date date = (Date) objectInputStream.readObject();
	System.out.println(date);
	System.out.println(objectInputStream.readBoolean());
	System.out.println(objectInputStream.readFloat());
	 
	// get the course object
	Filme filme = (Filme) objectInputStream.readObject();
	System.out.println(filme.getDescricao());

	objectInputStream.close();
	
	*/
		   
	}
	
	
		

}
